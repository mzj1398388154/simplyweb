import ReactDOM from 'react-dom';

function responsiveWithWidth(width){
    return document.body.offsetWidth<=width
}

function responsiveWithHeight(height){
    return document.body.offsetWidth<=height
}

class PopWindow{
    static show(height,component,callback){
        console.log(height)
        let check = document.getElementById('cover-page');
        if(check) return
        let div = document.createElement('div');
        div.setAttribute('id','cover-page');
        div.style.top = `${height || 0}px`
        if(component) ReactDOM.render(component,div)
        let root = document.getElementById('root');
        root.appendChild(div);
        div.onclick=(e)=>{
            if(e.target!==div) return
            if(callback){
                callback()
            }
            div.remove();
        }
    }

    static close(){
        let div = document.getElementById('cover-page');
        if(div) div.remove();
    }
}

export { responsiveWithWidth,responsiveWithHeight,PopWindow }