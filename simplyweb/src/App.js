import React,{Component} from 'react'
import './style/pages/app.css'
import './iconfont/iconfont.css'
import './style/components/app_transition.css'

import Container from './components/Container'
import HeaderTags from './components/HeaderTags'
import Progress from './components/DatasProgress'
import ContactUs from './components/Contact'
import Footer from './components/Footer'
// import Transition from './components/TransitionFadeIn'
import {CSSTransition} from 'react-transition-group'
import { responsiveWithWidth,PopWindow } from './utils/publicMethods'
import {logoTrans} from './components/CssSample'

class App extends Component{
    constructor(){
        super()
        this.state={
            responsive:false,
            tags:['HOME','ABOUT','PORTFOLIO','SERVICE','CONTACT'],
            services:[
                {
                    name:'Clinic Placement App',
                    img:'/images/static/1.jpg'
                },
                {
                    name:'Clinic Placement Website',
                    img:'/images/static/hours.png'
                },
                {
                    name:'Stewardship Finance Academy',
                    img:'/images/static/home.png'
                },
            ],
            skills:[
                {
                    name:'JavaScript',
                    rate:95
                  },
                  {
                    name:'Node.js',
                    rate:80
                  },
                  {
                    name:'React',
                    rate:85
                  },
                  {
                    name:'Python',
                    rate:75
                  },
                  {
                    name:'React Native',
                    rate:70
                  }
            ],
            member:[
                {
                    name:'Yin Peng',
                    nickName:'Perry',
                    experience:'2.5 Year',
                    skills:['Full Stack','SQL','Algorithm'],
                    skill_icon:['full-stack-pms','SQL','algorithm']
                },
                {
                    name:'Samuel',
                    nickName:'Sam',
                    experience:'2.5 Year',
                    skills:['User Interaction','Design','Front End'],
                    skill_icon:['user_interaction','designrightmenuiconactiveiconbeifen','front-endmonitoring']
                },
                {
                    name:'Andy Mei',
                    nickName:'Andy',
                    experience:'2.5 Year',
                    skills:['Front End','Back End','Machine Learning'],
                    skill_icon:['front-endmonitoring','full-stack-pms','machinelearning']
                },
                {
                    name:'Wang Luohao',
                    nickName:'Wency',
                    experience:'2.5 Year',
                    skills:['Back End','Data Mining','Algorithm'],
                    skill_icon:['front-endmonitoring','data-mining','algorithm']
                }
            ]
        }
    }

    //挂载的时候添加事件，一般对应的在下面，要移除监听时间
    componentDidMount(){
        let container = document.getElementsByClassName('container')
        let scrollRecord = []
        for(let i=0;i<container.length;i++){
            scrollRecord.push({height:container[i].offsetTop,at:false})
        }
        this.setState({scrollRecord,})
        window.addEventListener('scroll',this.handleScroll)
        this.getResponsive()
    }

    handleScroll = ()=>{
        let [...scrollRecord] = this.state.scrollRecord
        let remove = false;
        scrollRecord.forEach((el,index)=>{
            if(el.at) return
            if(window.scrollY+120>=el.height){
                el.at = true;
            }
            if(index===scrollRecord.length-1 && el.at) remove=true
        })
        if(remove){
            window.removeEventListener('scroll',this.handleScroll)
        }
        this.setState({
            scrollRecord,
        })
    }
    componentWillUnmount(){
        window.removeEventListener('resize',this.getResponsive)
    }

    getResponsive = ()=>{
        this.setState({
            //传宽度，小于此宽度responsive
            responsive:responsiveWithWidth(700)
        })
    }
    
    render(){
        const { 
            tags,
            services,
            responsive,
            scrollRecord,
            skills,
            member,
        } = this.state
        return (
            <div className='main'>
                <header>
                <img src="images/static/logo.png" alt="logo" />
                    <div className='header-tag'>
                        {/* 做成组件化是为了实现按区域加载，防止相同参数重复渲染 */}
                        <HeaderTags
                        tagList={tags}
                        response={responsive}
                        handleTagClick={this.scrollToAnchor} />
                    </div>
                </header>
                {/* 主要的格栏，可以传基本参数，以及anchor，
                anchor用于和上面的tag组成相对应的锚点，
                theme是定义背景和字体颜色，可以放 ‘dark’和‘light’
                默认排列是column */}
                <Container 
                style={{backgroundImage:['url("/images/static/globe.gif")']}}
                anchor={0}
                >
                    <div className='global-img' >
                        <h1 id='pageHero'>Hi, We Are SimplyWeb.</h1>
                    </div>
                </Container>
                <Container 
                    title='About Us.'
                    scrollAt = {true}
                    anchor={1}
                    style={{backgroundImage:['url("/images/static/white.jpg")']}}
                    theme='light'>
                    <div className='column-layout'>
                    <div className='column' style={{alignItems:"flex-start",padding:'0 50px',}}>
                        <span>Brief</span>
                        <p>
                        We are a team of 4, graduated from University of Queensland with various fields of IT majors. 
                        We strive to provide professional websites at an affordable price. We also ensure that we have clear communication within our team and to clients, in order to provide 
                        services that are of high work efficiency, quality, and flexibility. 
                        Furthermore, we had worked for more than 5 years together. Hence, all our codes are unified and well commented to ensure codes are professional and easy to read.
                        </p>
                        <div className='column team-area'>
                            {member.map((one,index)=>{
                                return (
                                    <CSSTransition
                                        key={index}
                                        in = {scrollRecord?scrollRecord[1].at:false}
                                        unmountOnExit
                                        classNames='card-transition'
                                        timeout = {1500}
                                        appear
                                    >
                                    <div onClick={
                                        ()=>PopWindow.show(window.scrollY,
                                        <div className='member-info'>
                                            <span 
                                            onClick={()=>PopWindow.close()}
                                            className='iconfont icon-guanbi' />
                                            <div>Name:<span>{one.name}</span></div>
                                            <div>Experience:<span>{one.experience}</span></div>
                                            <div>Focus On:</div>
                                            <div>{one.skills.map((el,index)=>{
                                                return (
                                                    <div key={index}>
                                                        <span className={`iconfont icon-${one.skill_icon[index]}`}/>{el}</div>
                                                )
                                            })}</div>
                                        </div>)
                                    }>
                                        <span className='iconfont icon-kuohao'/>
                                        {one.nickName}
                                        <span className='iconfont icon-kuohao' />
                                    </div>
                                    </CSSTransition>
                                )
                            })}
                        </div>
                    </div>
                    <div className='column' style={{alignItems:"flex-start"}}>
                        <span>Skills</span>
                        <Progress datas={skills} />
                    </div> 
                    </div>
                </Container>
                <Container 
                title='portfolio.'
                scrollAt={scrollRecord&&scrollRecord[2].at}
                anchor={2}
                style={{backgroundImage:['url("/images/static/black.jpg")']}}
                theme="dark"
                >
                    <CSSTransition
                        in = {scrollRecord?scrollRecord[2].at:false}
                        unmountOnExit
                        classNames='content-transition'
                        timeout = {1000}
                        appear
                    >
                    <div className='column-layout'>
                        {
                            services.map((service,index)=>{
                                return (
                                    <div className='column' key={index}>
                                        <button className = "portfolio" type = "submit" name = "portfolio">
                                            <img src={service.img} alt="service" />
                                        </button>
                                        <br/>
                                        <br/>
                                        <span>{service.name}</span>
                                    </div>
                                )
                            })
                        }
                    </div>
                    </CSSTransition>
                </Container>
                <Container 
                title='service.'
                scrollAt={scrollRecord&&scrollRecord[3].at}
                anchor={3}
                style={{backgroundImage:['url("/images/static/white.jpg")']}}
                theme="light"
                >
                    <CSSTransition
                        in = {scrollRecord?scrollRecord[3].at:false}
                        unmountOnExit
                        classNames='content-transition'
                        timeout = {1000}
                        appear
                    >
                    <div className = 'column-layout'>
                    {logoTrans({text:'INTERFACE DESIGN',backgroundImage:'design.jpg'})}
                    {logoTrans({text:'WEB DEVELOPMENT',backgroundImage:'polish.jpg'})}
                    {logoTrans({text:'MOBILE DEVELOPMENT',backgroundImage:'testing.jpg'})}
                    </div>
                    </CSSTransition>
                </Container>
                <Container 
                    title='Contact Us.'
                    scrollAt = {true}
                    anchor={4}
                    style={{backgroundImage:['url("/images/static/black.jpg")']}}
                    theme="dark"
                    >
                    <ContactUs />
                    <br/>
                    <br/>
                </Container>
                <Footer tags={tags} anchor={this.scrollToAnchor} />
            </div>  
        )
    }

    scrollToAnchor(anchor){
        if (anchor || anchor===0) {
            let anchorName = "anchor-"+anchor;
            let anchorElement = document.getElementById(anchorName);
            if(anchorElement) { anchorElement.scrollIntoView({block: 'start', behavior: 'smooth'}); }
        }
      }
}

export default App