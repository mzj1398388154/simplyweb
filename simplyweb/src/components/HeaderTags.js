import React, { Component } from 'react';
import '../style/pages/app.css';

class HeaderTags extends Component {
    constructor(){
        super()
        this.state={
            response:false,
            show:false
        }
    }

    //对比接受的参数，第一个是下一个从父类接受的参数，第二个是自己下一个state
    shouldComponentUpdate(nextProps,nextState){
        // 如果传参的response不变就不渲染
        if(nextProps.response !==this.state.response){
            this.setState({
                response:nextProps.response
            })
            return true
        }
        //如果state变了就要渲染，没这个的话下面的点击时间开关菜单就失效了
        else if(nextState.show !== this.state.show){
            return true;
        }
        else{
            return false
        }
    }

    render() {
        const { show } = this.state
        const { tagList, response,handleTagClick } = this.props
        //可以看这个render 出现了多少次来看是否重复渲染
        console.log('render')
        return (
            <>
            {
            // jsx写法里只有三元表达式，里面可以再嵌套，不能用if else
            response?
            <>
                {/* 阿里的iconfont，src下直接创iconfont文件，网上下了放替换文件就行了，按需加载，全部引入进来太大了 */}
                <span className='iconfont icon-caidan'
                    onClick={()=>this.setState({show:!show})}
                />
                <div 
                className='tag-list' 
                style={show?{transform:['scale(1)']}:{transform:['scale(0)']}}
                >
                    {tagList.map((tag, index) => {
                    return (
                        <div 
                            key={index}
                            onClick={() => {
                                this.setState({show:false})
                                handleTagClick(index)}
                            }
                        >
                            {tag}
                        </div>
                    )
                })}
                </div>
            </>
            :
                tagList.map((tag, index) => {
                    return (
                        <div
                            key={index}
                            onClick={() => handleTagClick(index)}
                        >
                            {tag}
                        </div>
                    )
                })
            }
            </>
        )
    }
}

export default HeaderTags;
