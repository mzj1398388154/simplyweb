import React,{Component} from 'react';
import '../style/pages/app.css';
import {CSSTransition} from 'react-transition-group'

class Container extends Component {
  render(){
      const { children,theme,style,anchor,title,scrollAt=false } = this.props
    return (
      <>
      <div id={`anchor-${anchor}`}></div>
      <section 
      style={style}
      className={`container container-${theme}`}>
        {title &&
        <CSSTransition
        in = {scrollAt}
        unmountOnExit
        classNames='content-title-transition'
        timeout = {1000}
        appear
        >
        <h1 title-text={title}>{title}</h1>
        </CSSTransition>
        }
        {children}
      </section>
      </>
    )
  }
}

export default Container;
