import React,{Component} from 'react';
import '../style/components/progress.css';

class Datas extends Component {
  render(){
      const { datas=[] } = this.props
    return (
      <div className='data-progress'>
        {datas.map((data,index)=>{
          return (<div className='data-progress-area' key={index}>
          <label>{data.name}</label>
          <span className='progress-label' 
          style={{left:`${data.rate}%`}}>
            {data.rate}
            </span>
          <progress value={data.rate} max="100" />
          </div>)
        })}
      </div>
    )
  }
}

export default Datas;
