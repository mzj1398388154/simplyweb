import React,{Component} from 'react';
import '../style/components/footer.css';

class Footer extends Component {
    render(){
        const {tags=[],anchor} = this.props
       return (
            <footer>
                <div>
                    {tags.map((tag,index)=>{
                        return !index?<></>:<div
                        key={index}
                        onClick={() => anchor(index)}
                        >
                            {tag}
                        </div>
                    })}
                </div>
                <div>
                    <div>Copyright © 2020.Group Four Simplyweb All rights reserved.</div>
                </div>
                <span className="iconfont icon-fanhuidingbu" onClick={()=>anchor(0)} />
            </footer>
       )
    }
  }
  
  export default Footer;