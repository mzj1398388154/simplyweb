import React from 'react';
import '../style/components/transition.css';

function logoTrans(props){
    return (
        <div className="transition-main" style={{backgroundImage:['url("/images/static/'+ props.backgroundImage +'")']}}>
            <h2>{props.title}</h2>
            <h1 className='spotlight' data-text={props.text}>
                {props.text}
            </h1>
        </div>
    )
}

function lightHover(props){
    return (
        <div className="transition-main"  style={{backgroundImage:['url("/images/static/'+ props.backgroundImage +'")']}}>
            <h2>{props.title}</h2>
            <div className='light-hover'>
                {props.name}
            </div>
        </div>
    )
} 

function buttonLight(props){
    return (
        <div className="transition-main"  style={{backgroundImage:['url("/images/static/'+ props.backgroundImage +'")']}}>
            <h2>{props.title}</h2>
            <button className='light-button' onClick={(e)=>{
                let span = document.createElement('span')
                e.currentTarget.appendChild(span)
                setTimeout(()=>{
                    span.remove()
                },500)
                props.onClick && props.onClick()
            }}>
                {props.text}
            </button>
        </div>
    )
}

export {logoTrans,lightHover,buttonLight}
