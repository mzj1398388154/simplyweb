import React,{Component} from 'react';
import {CSSTransition} from 'react-transition-group'
import '../style/components/app_transition.css';

class Tran extends Component {
  render(){
      const { children,start=false,appear=false,timeout=1000,classNames } = this.props
    return (
        <CSSTransition
          in={start}
          unmountOnExit
          classNames={classNames}
          timeout = {timeout}
          appear = {appear} 
        >
            {children}
        </CSSTransition>
    )
  }
}

export default Tran;
