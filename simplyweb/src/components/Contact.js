import React from 'react';
import emailjs from 'emailjs-com';
import '../style/components/contact.css'


export default function ContactUs() {

  function sendEmail(e) {
    e.preventDefault();

    emailjs.sendForm('gmail', 'form', e.target, 'user_5w86WCaPyxhqB72NbyF0r')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });
      e.target.reset();
  }

  return (
    <form className="contact-form" onSubmit={sendEmail}>
      <h2>Have a Project?</h2>
      <div id="input-wrap">
        <div className='input-label' style={{width:'47%', marginRight:'3%'}}>
          <label>Your Name</label>
          <input type="text" placeholder = "Name" name="name" required style={{width:'94%'}} />
        </div>
        <div className='input-label' style={{flex:1, width:'47%', marginLeft:'3%'}}>
          <label>Your Email</label>
          <input type="email" placeholder = "Email Address" name="email" required style={{width:'94%'}} />
        </div>
      </div>
      <br/>
      <label>Justification</label>
      <textarea name="message" placeholder= "Your Message" required />
      <input type="submit" value="Send" />
    </form>
  );
}